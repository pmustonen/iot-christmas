#include <string.h>
#include "esp_log.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"

#include "httpclient.h"

// The silliest HTTP "client" ever:

static const char* TAG = "httpclient";
static char recv_buf[RECV_BUF_SIZE];

void http_init(HttpClient_t *client, HttpCallback_t http_connected_cb, HttpCallback_t http_chunk_cb, HttpCallback_t http_disconnected_cb)
{
  //memset(client, 0, sizeof(client));
  //client->recv_buf = (char* )malloc(RECV_BUF_SIZE * sizeof(char));
  client->recv_buf = recv_buf;
  client->recv_buf_size = RECV_BUF_SIZE;
  client->http_connected_cb = http_connected_cb;
  client->http_chunk_cb = http_chunk_cb;
  client->http_disconnected_cb = http_disconnected_cb;
}

void http_deinit(HttpClient_t *client)
{
  //free(client->recv_buf);
  memset(client->recv_buf, 0, RECV_BUF_SIZE * sizeof(char));
  free(client->proc_buf);
}

HttpError_t http_make_request(HttpClient_t *client, const char *web_server, const char *port, const char *req_str)
{
  const struct addrinfo hints = {
    .ai_family = AF_INET,
    .ai_socktype = SOCK_STREAM,
  };

  struct addrinfo *res;
  struct in_addr *addr;
  int s, r;

  int err = getaddrinfo(web_server, port, &hints, &res);

  if (err != 0 || res == NULL)
  {
    ESP_LOGE(TAG, "DNS lookup failed err=%d res=%p", err, res);
    return HE_DNS;
  }

  addr = &((struct sockaddr_in *) res->ai_addr)->sin_addr;
  ESP_LOGI(TAG, "DNS lookup succeeded. IP=%s", inet_ntoa(*addr));

  s = socket(res->ai_family, res->ai_socktype, 0);
  if (s < 0) {
    ESP_LOGE(TAG, "Failed to allocate socket");
    freeaddrinfo(res);
    return HE_SOCKET;
  }
  ESP_LOGI(TAG, "Allocated socket");

  // try connecting to given server
  if (connect(s, res->ai_addr, res->ai_addrlen) != 0) {
    ESP_LOGE(TAG, "failed to connect, error=%d", errno);
    close(s);
    freeaddrinfo(res);
    return HE_CONNECT;
  }

  ESP_LOGI(TAG, "Connected!");
  freeaddrinfo(res);
  if (client->http_connected_cb) {
    client->http_connected_cb(client);
  }

  if (write(s, req_str, strlen(req_str)) < 0) {
     ESP_LOGE(TAG, "socket send failed");
     close(s);
     return HE_COMM;
  }

  ESP_LOGI(TAG, "... socket send success");

  struct timeval receiving_timeout;
  receiving_timeout.tv_sec = 5;
  receiving_timeout.tv_usec = 0;
  if (setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, &receiving_timeout,
         sizeof(receiving_timeout)) < 0) {
    ESP_LOGE(TAG, "failed to set socket receiving timeout");
    close(s);
    return HE_COMM;
  }
  ESP_LOGI(TAG, "... set socket receiving timeout success");

  /* Read HTTP response */
  do {
     bzero(client->recv_buf, sizeof(*(client->recv_buf) * client->recv_buf_size));
     r = read(s, client->recv_buf, sizeof(*(client->recv_buf)) * client->recv_buf_size - 1);
     if (client->http_chunk_cb) {
       client->http_chunk_cb(client);
     }
  } while(r > 0);

  ESP_LOGI(TAG, "... done reading from socket. Last read return=%d errno=%d\r\n", r, errno);
  close(s);

  if (client->http_disconnected_cb) {
    client->http_disconnected_cb(client);
  }

  return HE_NONE;
}

const char* http_find_response_body(char *response)
{
    // Identify end of the response headers
    // http://stackoverflow.com/questions/11254037/how-to-know-when-the-http-headers-part-is-ended
    char *eol; // end of line
    char *bol; // beginning of line
    bool nheaderfound = false; // end of response headers has been found

    bol = response;
    while ((eol = index(bol, '\n')) != NULL) {
        // update bol based upon the value of eol
        bol = eol + 1;
        // test if end of headers has been reached
        if ((!(strncmp(bol, "\r\n", 2))) || (!(strncmp(bol, "\n", 1))))
        {
           // note that end of headers has been found
            nheaderfound = true;
           // update the value of bol to reflect the beginning of the line
           // immediately after the headers
           if (bol[0] != '\n') {
              bol += 1;
           }
           bol += 1;
           break;
        }
    }
    if (nheaderfound) {
        return bol;
    } else {
        return NULL;
    }
}
