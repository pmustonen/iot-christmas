#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_log.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"

#include "wifi.h"
#include "httpclient.h"
#include "rainbow.h"

// TODO: These _shouldn't_ be hard-coded
#define WEB_SERVER "10.175.20.135"
#define WEB_PORT "80"
#define WEB_URL "http://10.175.20.135/get_settings"

static void wifi_print_information(void);

static const char* TAG = "wifi";
/* FreeRTOS event group to signal when we are connected & ready to make a request */
static EventGroupHandle_t wifi_event_group;

/* The event group allows multiple bits for each event,
   but we only care about one event - are we connected
   to the AP with an IP? */
const int CONNECTED_BIT = BIT0;

static esp_err_t wifi_event_handler(void *ctx, system_event_t *event)
{
    switch(event->event_id) {
    case SYSTEM_EVENT_STA_START:
        esp_wifi_connect();
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        /* This is a workaround as ESP32 WiFi libs don't currently
           auto-reassociate. */
        esp_wifi_connect();
        xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
        break;
    default:
        break;
    }
    return ESP_OK;
}

void wifi_app_init(void)
{
  tcpip_adapter_init();
  wifi_event_group = xEventGroupCreate();
  ESP_ERROR_CHECK( esp_event_loop_init(wifi_event_handler, NULL) );
  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
  ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_RAM) );

  wifi_config_t wifi_config = {
      .sta = {
          .ssid = WIFI_SSID,
          .password = WIFI_PASS,
      },
  };
  ESP_LOGI(TAG, "Setting WiFi configuration SSID %s...", wifi_config.sta.ssid);
  ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA) );
  ESP_ERROR_CHECK( esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
  ESP_ERROR_CHECK( esp_wifi_start() );
}

static void process_chunk(void *args)
{
    HttpClient_t* client = (HttpClient_t*)args;

    int proc_buf_new_size = client->proc_buf_size + client->recv_buf_size;
    char *copy_from;

    if (client->proc_buf == NULL){
        client->proc_buf = malloc(proc_buf_new_size);
        copy_from = client->proc_buf;
    } else {
        proc_buf_new_size -= 1; // chunks of data are '\0' terminated
        client->proc_buf = realloc(client->proc_buf, proc_buf_new_size);
        copy_from = client->proc_buf + proc_buf_new_size - client->recv_buf_size;
    }

    if (client->proc_buf == NULL) {
        ESP_LOGE(TAG, "Failed to allocate memory");
    }

    client->proc_buf_size = proc_buf_new_size;
    memcpy(copy_from, client->recv_buf, client->recv_buf_size);
    ESP_LOGI(TAG, "chunk processed");
}

static void process_chunk_static(void *args)
{
  HttpClient_t* client = (HttpClient_t*)args;

}

static void disconnected(void *args)
{
    HttpClient_t *client = (HttpClient_t *)args;

    const char *response_body = http_find_response_body(client->proc_buf);
    ESP_LOGI(TAG, "response: %s", client->proc_buf);
    if (response_body)
    {
      ESP_LOGI(TAG, "response body: %s", response_body);
      rainbow_update_json(response_body);
    }
    else {
      ESP_LOGE(TAG, "No response body");
    }

    http_deinit(client);
    ESP_LOGD(TAG, "Free heap %u", xPortGetFreeHeapSize());
}


void wifi_make_request(void *pvParameters)
{
  static const char *req_str = "GET " WEB_URL " HTTP/1.0\r\n"
      "Host: "WEB_SERVER"\r\n"
      "User-Agent: esp-idf/1.0 esp32\r\n"
      "\r\n";

  xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT,
                    false, true, portMAX_DELAY);
  ESP_LOGI(TAG, "Connected to AP");
  wifi_print_information();
  while (1)
  {
    ESP_LOGD(TAG, "one round in loop")
    HttpClient_t client = {0};
    http_init(&client, NULL, &process_chunk, &disconnected);
    http_make_request(&client, WEB_SERVER, WEB_PORT, req_str);
    ESP_LOGD(TAG, "Request made, going to sleeeeeep.")
    vTaskDelay(8000 / portTICK_PERIOD_MS);
    ESP_LOGD(TAG, "Waking up!");
  }

  vTaskDelete(NULL);
}

static void wifi_print_information(void)
{
  tcpip_adapter_ip_info_t ip = {0};
  if (tcpip_adapter_get_ip_info(ESP_IF_WIFI_STA, &ip) == 0) {
    ESP_LOGI(TAG, "~~~~~~~~~~~");
    ESP_LOGI(TAG, "IP:"IPSTR, IP2STR(&ip.ip));
    ESP_LOGI(TAG, "MASK:"IPSTR, IP2STR(&ip.netmask));
    ESP_LOGI(TAG, "GW:"IPSTR, IP2STR(&ip.gw));
    ESP_LOGI(TAG, "~~~~~~~~~~~");
  }
  else {
    ESP_LOGE(TAG, "No IP connectivity");
  }
}
