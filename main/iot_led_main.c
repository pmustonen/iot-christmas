/* IOT Christmas Decoration
 *
 *
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "rainbow.h"
#include "wifi.h"

static const char* TAG = "main";
static void test_task(void *pvParameter);


void app_main()
{
    ESP_ERROR_CHECK(nvs_flash_init());

    RainbowSettings_t rainbow = {
      .max_brightness = 250,
      .color_div = 4,
      .anim_step = 12,
      .delay = 200
    };

    rainbow_init(&rainbow);
    xTaskCreate(&rainbow_task, "rainbow_task", 4096, NULL, 5, NULL);
    wifi_app_init();
    xTaskCreate(&wifi_make_request, "wifi_task", 8192, NULL, 5, NULL);
    //xTaskCreate(&test_task, "test_task", 4096, NULL, 5, NULL);
}

static void test_task(void *pvParameter) {
  RainbowSettings_t test_settings = {
    .max_brightness = 230,
    .color_div = 3,
    .anim_step = 10,
    .delay = 100
  };
  char *jsontest = "{\"brightness\": 250, \"color_div\": 12, \"anim_step\": 5, \"delay\": 90}";
  vTaskDelay(10000 / portTICK_PERIOD_MS);
  rainbow_update(&test_settings);
  vTaskDelay(10000 / portTICK_PERIOD_MS);
  rainbow_update_json(jsontest);
  vTaskDelay(10000 / portTICK_PERIOD_MS);
  test_settings.max_brightness = 250;
  rainbow_update(&test_settings);
  vTaskDelay(10000 / portTICK_PERIOD_MS);
  test_settings.color_div = 5;
  test_settings.anim_step = 8;
  test_settings.delay = 80;
  rainbow_update(&test_settings);
  vTaskDelay(10000 / portTICK_PERIOD_MS);
  vTaskDelete(NULL);
}
