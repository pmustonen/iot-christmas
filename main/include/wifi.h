#ifndef _WIFI_H__
#define _WIFI_H__

// WIFI SSID and passphrase should be set with "make menuconfig"
#define WIFI_SSID CONFIG_WIFI_SSID
#define WIFI_PASS CONFIG_WIFI_PASSWORD

void wifi_app_init(void);
void wifi_make_request(void *pvParameters);

#endif
