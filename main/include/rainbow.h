#ifndef _RAINBOW_H__
#define _RAINBOW_H__

#include "freertos/queue.h"


#define DATA_PIN 18 // Avoid using any of the strapping pins on the ESP32
#define NUM_PIXELS 50  // How many pixels you want to drive

typedef struct {
  uint8_t max_brightness;
  uint8_t color_div;
  uint8_t anim_step;
  uint16_t delay;
} RainbowSettings_t;

void rainbow_init(RainbowSettings_t *new_settings);
void rainbow_task(void *pvParameter);
void rainbow_update(RainbowSettings_t *new_settings);
void rainbow_update_json(char *json_settings);

#endif
