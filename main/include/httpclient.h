#ifndef _HTTPCLIENT_H__
#define _HTTPCLIENT_H__

#define RECV_BUF_SIZE 1024

typedef enum {
  HE_DNS,
  HE_SOCKET,
  HE_CONNECT,
  HE_COMM,
  HE_NONE
} HttpError_t;

typedef void (*HttpCallback_t)(void *client);

typedef struct {
  char *recv_buf;
  int recv_buf_size;
  char *proc_buf;
  int proc_buf_size;
  HttpCallback_t http_connected_cb;
  HttpCallback_t http_chunk_cb;
  HttpCallback_t http_disconnected_cb;
} HttpClient_t;

void http_init(HttpClient_t *client, HttpCallback_t http_connected_cb, HttpCallback_t http_chunk_cb, HttpCallback_t http_disconnected_cb);
void http_deinit(HttpClient_t *client);
HttpError_t http_make_request(HttpClient_t *client, const char *web_server, const char *port, const char *req_str);
const char* http_find_response_body(char *response);

#endif
