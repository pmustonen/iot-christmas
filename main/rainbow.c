#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_log.h"

#include "cJSON.h"

#include "rainbow.h"
#include "ws2812.h"

static void rainbow_off();

static const char* TAG = "rainbow";
static rgbVal pixels[NUM_PIXELS];
static RainbowSettings_t settings = {0};
static QueueHandle_t rainbowQueue;

void rainbow_init(RainbowSettings_t *new_settings)
{
  rainbowQueue = xQueueCreate(3, sizeof(RainbowSettings_t));
  ESP_LOGI(TAG, "Initializing rainbow...");
  if (ws2812_init(DATA_PIN, LED_WS2812B)) {
    ESP_LOGE(TAG, "WS2812 init failure");
  }
  rainbow_off();
  settings = *new_settings;
  ESP_LOGI(TAG, "Success!")
}

void rainbow_update(RainbowSettings_t *new_settings)
{
  // this is meant to be called from other tasks than rainbow_task!!
  // enqueue the update, use deep-copy, don't block if queue is full.
  xQueueSend(rainbowQueue, (void *) new_settings, (TickType_t) 0);
  ESP_LOGI(TAG, "Queuing updates...");
}

void rainbow_update_json(char *json_settings)
{
  ESP_LOGI(TAG, "Got some JSON: %s", json_settings);
  // TODO: verify something before casting!
  cJSON *root = cJSON_Parse(json_settings);
  RainbowSettings_t new_settings = {
    .max_brightness = (uint8_t) cJSON_GetObjectItem(root, "brightness")->valueint,
    .color_div = (uint8_t) cJSON_GetObjectItem(root, "color_div")->valueint,
    .anim_step = (uint8_t) cJSON_GetObjectItem(root, "anim_step")->valueint,
    .delay = (uint16_t) cJSON_GetObjectItem(root, "delay")->valueint
  };

  rainbow_update(&new_settings);
  cJSON_Delete(root);
}

void rainbow_task(void *pvParameter)
{
  RainbowSettings_t new_settings = {};
  const uint8_t anim_max = settings.max_brightness - settings.anim_step;
  rgbVal color = makeRGBVal(anim_max, 0, 0);
  rgbVal color2 = makeRGBVal(anim_max, 0, 0);
  uint8_t stepVal = 0;
  uint8_t stepVal2 = 0;

  while (1) {
    // Check, whether we have updated values waiting, don't block.
    if (xQueueReceive(rainbowQueue, &(new_settings), (TickType_t) 0))
    {
      settings = new_settings;
      ESP_LOGI(TAG, "Got new settings");
    }

    color = color2;
    stepVal = stepVal2;

    for (uint16_t i = 0; i < NUM_PIXELS; i++) {
      pixels[i] = makeRGBVal(color.r/settings.color_div, color.g/settings.color_div, color.b/settings.color_div);

      if (i == 1) {
        color2 = color;
        stepVal2 = stepVal;
      }

      switch (stepVal) {
        case 0:
        color.g += settings.anim_step;
        if (color.g >= anim_max)
          stepVal++;
        break;
        case 1:
        color.r -= settings.anim_step;
        if (color.r == 0)
          stepVal++;
        break;
        case 2:
        color.b += settings.anim_step;
        if (color.b >= anim_max)
          stepVal++;
        break;
        case 3:
        color.g -= settings.anim_step;
        if (color.g == 0)
          stepVal++;
        break;
        case 4:
        color.r += settings.anim_step;
        if (color.r >= anim_max)
          stepVal++;
        break;
        case 5:
        color.b -= settings.anim_step;
        if (color.b == 0)
          stepVal = 0;
        break;
      }
    }

    ws2812_setColors(NUM_PIXELS, pixels);

    vTaskDelay(settings.delay / portTICK_PERIOD_MS);
  }
}

static void rainbow_off() {
  for (int i = 0; i < NUM_PIXELS; i++) {
    pixels[i] = makeRGBVal(0, 0, 0);
  }
  ws2812_setColors(2, pixels);
  //ws2812_setColors(NUM_PIXELS, pixels);
}
